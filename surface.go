package main

import (
	"image/color"

	"gioui.org/f32"
	"gioui.org/layout"
	"gioui.org/op"
	"gioui.org/op/clip"
	"gioui.org/op/paint"
	"gioui.org/unit"
	"gioui.org/widget/material"
)

type Surface struct {
	Elevation unit.Value
}

func (s Surface) Layout(gtx layout.Context, th *material.Theme, child layout.Widget) layout.Dimensions {
	cgtx := gtx
	//defer op.Save(gtx.Ops).Load()
	macro := op.Record(gtx.Ops)
	dims := child(cgtx)
	call := macro.Stop()
	sz := dims.Size

	r := f32.Rect(0, 0, float32(sz.X), float32(sz.Y))
	clip.UniformRRect(r, 5).Add(gtx.Ops)
	c := shadeColor(color.NRGBA{
		A: 0xff,
		R: 0x12,
		G: 0x12,
		B: 0x12,
	}, int(s.Elevation.V*40))

	paint.Fill(gtx.Ops, c)
	call.Add(gtx.Ops)

	return layout.Dimensions{Size: sz}
}
