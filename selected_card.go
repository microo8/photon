package main

import (
	"image"
	"math"
)

var (
	selectedCard = &SelectedCard{}
)

type SelectedCard struct {
	*Card
	Pos image.Point
}

func (sc *SelectedCard) MoveLeft() {
	if selectedCard == nil {
		return
	}
	defer sc.Refresh()
	if sc.Pos.X == 0 {
		if sc.Pos.Y == 0 {
			return
		}
		sc.Pos.Y--
		if list.Position.First == sc.Pos.Y {
			list.Position.Offset = 0
		}
		if list.Position.First > sc.Pos.Y {
			list.Position.First = sc.Pos.Y
		}
		sc.Pos.X = ncols - 1
		return
	}
	sc.Pos.X--
}

func (sc *SelectedCard) MoveRight() {
	if selectedCard == nil {
		return
	}
	defer sc.Refresh()
	if sc.Pos.Y == int(math.Ceil(float64(len(visibleCards))/float64(ncols)))-1 {
		if sc.Pos.X == ncols-1 || len(visibleCards) == (sc.Pos.Y*ncols+sc.Pos.X+1) {
			return
		}
		sc.Pos.X++
		return
	}
	if sc.Pos.X == ncols-1 {
		sc.Pos.Y++
		sc.Pos.X = 0
		if list.Position.First+list.Position.Count-1 < sc.Pos.Y {
			list.Position.First++
		}
		if list.Position.First+list.Position.Count-1 == sc.Pos.Y {
			list.Position.Offset -= list.Position.OffsetLast
			list.Position.OffsetLast = 0
		}
		return
	}
	sc.Pos.X++
}

func (sc *SelectedCard) MoveDown() {
	if selectedCard == nil {
		return
	}
	defer sc.Refresh()
	maxRow := int(math.Ceil(float64(len(visibleCards))/float64(ncols))) - 1
	if sc.Pos.Y == maxRow {
		return
	}
	sc.Pos.Y++
	if sc.Pos.Y == maxRow {
		sc.Pos.X = min(sc.Pos.X, len(visibleCards)%ncols-1)
	}
	if list.Position.First+list.Position.Count-1 == sc.Pos.Y {
		list.Position.Offset -= list.Position.OffsetLast
		list.Position.OffsetLast = 0
	}
	if list.Position.First+list.Position.Count-1 < sc.Pos.Y {
		list.Position.First++
	}
}

func (sc *SelectedCard) MoveUp() {
	if selectedCard == nil {
		return
	}
	defer sc.Refresh()
	if sc.Pos.Y == 0 {
		return
	}
	sc.Pos.Y--
	if list.Position.First == sc.Pos.Y {
		list.Position.Offset = 0
	}
	if list.Position.First > sc.Pos.Y {
		list.Position.First = sc.Pos.Y
	}
}

func (sc *SelectedCard) Refresh() {
	if sc.Pos.Y < 0 {
		sc.Pos.Y = 0
	}
	if sc.Pos.X < 0 {
		sc.Pos.X = 0
	}
	index := sc.Pos.Y*ncols + sc.Pos.X
	if len(visibleCards)-1 < index {
		selectedCard.Card = nil
		return
	}
	selectedCard.Card = visibleCards[index]
}
