package main

import (
	"bytes"
	"log"
	"os"
	"os/exec"
	"os/user"
	"path/filepath"
	"sort"
	"strings"

	"gioui.org/app"
	"github.com/alecthomas/kong"

	"github.com/mmcdole/gofeed"
	"gitlab.com/microo8/photon/internal/events"
	"gitlab.com/microo8/photon/internal/inputs"
	"gitlab.com/microo8/photon/internal/media"
)

var CLI struct {
	Extractor    string       `optional default:"youtube-dl --get-url %" help:"command for media link extraction (item link is substituted for %)" env:"PHOTON_EXTRACTOR"`
	VideoCmd     string       `optional default:"mpv %" help:"set default command for opening the item media link in a video player (media link is substituted for %, if no % is provided, photon will download the data and pipe it to the stdin of the command)" env:"PHOTON_VIDEOCMD"`
	ImageCmd     string       `optional default:"imv -" help:"set default command for opening the item media link in a image viewer (media link is substituted for %, if no % is provided, photon will download the data and pipe it to the stdin of the command)" env:"PHOTON_IMAGECMD"`
	TorrentCmd   string       `optional default:"mpv %" help:"set default command for opening the item media link in a torrent downloader (media link is substituted for %, if link is a torrent file, photon will download it, and substitute the torrent file path for %)" env:"PHOTON_TORRENTCMD"`
	HTTPSettings HTTPSettings `embed`
	Paths        []string     `arg optional help:"RSS/Atom urls, config path, or - for stdin"`
	DownloadPath string       `optional default:"$HOME/Downloads" help:"the default download path"`
}

var (
	feedInputs     inputs.Inputs
	imgDownloader  *ImgDownloader
	mediaExtractor *media.Extractor
)

func main() {
	//prof := profile.Start(profile.ProfilePath("."))
	log.SetFlags(log.Lshortfile | log.Ltime)
	kong.Parse(&CLI,
		kong.Name("photon"),
		kong.Description("Fast RSS reader as light as a photon"),
		kong.UsageOnError(),
		kong.ConfigureHelp(kong.HelpOptions{
			Compact: true,
			Summary: true,
		}))

	err := LoadPlugins()
	if err != nil {
		log.Fatal("ERROR:", err)
	}

	feedInputs, err = loadFeeds()
	if err != nil {
		log.Fatal(err)
	}
	if len(feedInputs) == 0 {
		log.Fatal("no feeds")
	}

	events.Emit(&events.Init{})

	mediaExtractor = &media.Extractor{
		ExtractorCmd: CLI.Extractor,
		VideoCmd:     CLI.VideoCmd,
		ImageCmd:     CLI.ImageCmd,
		TorrentCmd:   CLI.TorrentCmd,
		Client:       CLI.HTTPSettings.Client(),
	}
	imgDownloader = NewImgDownloader(WithClient(CLI.HTTPSettings.Client()))
	w := app.NewWindow(app.Title("photon"))
	defaultKeyBindings(w)

	go refreshFeed(w)
	go func() {
		defer func() {
			if r := recover(); r != nil {
				log.Panic(r)
			}
			//prof.Stop()
			os.Exit(0)
		}()
		if err := guiLoop(w); err != nil {
			log.Fatal(err)
		}
	}()
	app.Main()
}

func loadFeeds() (inputs.Inputs, error) {
	if len(CLI.Paths) == 0 {
		usr, err := user.Current()
		if err != nil {
			return nil, err
		}
		defaultConf := filepath.Join(usr.HomeDir, ".config", "photon", "config")
		if _, err := os.Stat(defaultConf); os.IsNotExist(err) {
			return nil, nil
		}
		CLI.Paths = []string{defaultConf}
	}
	var ret []string
	for _, path := range CLI.Paths {
		switch {
		case path == "-":
			if len(CLI.Paths) > 1 {
				log.Fatal("ERROR: cannot parse from args and stdin")
			}
			ret = append(ret, "-")
		case strings.HasPrefix(path, "http://"), strings.HasPrefix(path, "https://"), strings.HasPrefix(path, "cmd://"):
			ret = append(ret, path)
		default:
			f, err := os.Open(path)
			if err != nil {
				log.Fatal("ERROR: opening file:", err)
			}
			defer f.Close()
			feeds, err := inputs.Parse(f)
			if err != nil {
				log.Fatal("ERROR: parsing file:", err)
			}
			ret = append(ret, feeds...)
		}
	}
	return ret, nil
}

func refreshFeed(w *app.Window) {
	cards = nil
	feeds := make(chan *gofeed.Feed)
	for _, feedURL := range feedInputs {
		feedURL := feedURL
		go func() {
			fp := gofeed.NewParser()
			fp.Client = CLI.HTTPSettings.Client()
			fp.AtomTranslator = NewCustomAtomTranslator()
			fp.RSSTranslator = NewCustomRSSTranslator()
			var err error
			var f *gofeed.Feed
			switch {
			case feedURL == "-":
				f, err = fp.Parse(os.Stdin)
			case strings.HasPrefix(feedURL, "cmd://"):
				var command []string
				for _, c := range strings.Split(feedURL[6:], " ") {
					c = strings.TrimSpace(c)
					if c != "" {
						command = append(command, c)
					}
				}
				cmd := exec.Command(command[0], command[1:]...)
				var stdout bytes.Buffer
				cmd.Stdout = &stdout
				if err := cmd.Run(); err != nil {
					log.Printf("ERROR: running command (%s): %s", feedURL, err)
					feeds <- nil
					return
				}
				f, err = fp.Parse(&stdout)
			case strings.HasPrefix(feedURL, "http://"), strings.HasPrefix(feedURL, "https://"):
				f, err = fp.ParseURL(feedURL)
			default:
				log.Fatalf("ERROR: not supported feed: %s", feedURL)
			}
			if err != nil {
				log.Printf("ERROR: downloading feed (%s): %s", feedURL, err)
				feeds <- nil
				return
			}
			if f.Image != nil && f.Image.URL != "" {
				imgDownloader.Download(f.Image.URL, nil)
			}
			feeds <- f
		}()
	}
	var feedsGot int
	for f := range feeds {
		feedsGot++
		if f == nil {
			if feedsGot == len(feedInputs) {
				break
			}
			continue
		}
		newCards := make(Cards, len(f.Items))
		for i, item := range f.Items {
			newCards[i] = &Card{
				Item:   item,
				Feed:   f,
				Window: w,
			}
		}
		cards = append(cards, newCards...)
		if feedsGot == len(feedInputs) {
			break
		}
	}
	sort.Sort(cards)
	if searchEditor != nil {
		filterCards(searchEditor.Text())
	} else {
		visibleCards = cards
	}
	if len(visibleCards) > 0 {
		selectedCard.Card = visibleCards[0]
	}
	events.Emit(&events.FeedsDownloaded{})
	w.Invalidate()
}
