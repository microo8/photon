package states

type StateEnum int

const (
	Normal StateEnum = iota
	Article
	Search
)
