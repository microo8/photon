package keybindings

import (
	lua "github.com/yuin/gopher-lua"
	"gitlab.com/microo8/photon/internal/states"
)

func Loader(kb *Registry) lua.LGFunction {
	return func(L *lua.LState) int {
		var exports = map[string]lua.LGFunction{
			"add": keybindingsAdd(kb),
		}
		mod := L.SetFuncs(L.NewTable(), exports)
		L.Push(mod)

		return 1
	}
}

func keybindingsAdd(kb *Registry) lua.LGFunction {
	return func(L *lua.LState) int {
		state := states.StateEnum(L.ToNumber(1))
		keyString := L.ToString(2)
		fn := L.ToFunction(3)
		kb.Add(state, keyString, func() error {
			L.Push(fn)
			return L.PCall(0, lua.MultRet, nil)
		})
		return 0
	}
}
