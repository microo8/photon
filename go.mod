module gitlab.com/microo8/photon

go 1.16

require (
	gioui.org v0.0.0-20210520085948-5f631209eadd
	github.com/PuerkitoBio/goquery v1.6.1 // indirect
	github.com/alecthomas/kong v0.2.16
	github.com/andybalholm/cascadia v1.2.0 // indirect
	github.com/cixtor/readability v1.0.0
	github.com/json-iterator/go v1.1.11 // indirect
	github.com/mmcdole/gofeed v1.1.3
	github.com/mmcdole/goxpp v0.0.0-20200921145534-2f3784f67354 // indirect
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/modern-go/reflect2 v1.0.1 // indirect
	github.com/pkg/browser v0.0.0-20210115035449-ce105d075bb4
	github.com/pkg/errors v0.9.1 // indirect
	github.com/sbani/go-humanizer v0.3.1
	github.com/stretchr/testify v1.7.0 // indirect
	github.com/yuin/gopher-lua v0.0.0-20200816102855-ee81675732da
	gitlab.com/microo8/richtext v0.0.0-20210524161724-107dc715fbae
	golang.design/x/clipboard v0.4.6
	golang.org/x/exp v0.0.0-20210514180818-737f94c0881e
	golang.org/x/image v0.0.0-20210504121937-7319ad40d33e
	golang.org/x/net v0.0.0-20210521195947-fe42d452be8f
	golang.org/x/sys v0.0.0-20210521203332-0cec03c779c1 // indirect
	golang.org/x/text v0.3.6 // indirect
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b // indirect
)
