package main

import (
	"image"
	"io"
	"log"
	"mime"
	"net/http"
	"net/url"
	"os"
	"os/user"
	"path/filepath"
	"strings"
	"time"

	"gioui.org/app"
	"gioui.org/f32"
	"gioui.org/io/key"
	"gioui.org/io/pointer"
	"gioui.org/layout"
	"gioui.org/op/clip"
	"gioui.org/op/paint"
	"gioui.org/text"
	"gioui.org/unit"
	"gioui.org/widget"
	"gioui.org/widget/material"
	"github.com/cixtor/readability"
	"github.com/mmcdole/gofeed"
	"github.com/pkg/browser"
	"gitlab.com/microo8/photon/internal/events"
	"gitlab.com/microo8/photon/internal/media"
	"gitlab.com/microo8/richtext"
	"golang.org/x/image/draw"

	htime "github.com/sbani/go-humanizer/time"
)

var (
	cardPadding = unit.Dp(10)
	insetValue  = unit.Px(3)
)

type Card struct {
	Item               *gofeed.Item
	ItemImage          image.Image
	ItemImageOp        paint.ImageOp
	Feed               *gofeed.Feed
	FeedImage          image.Image
	FeedImageOp        paint.ImageOp
	Article            *readability.Article
	ArticleRichContent richtext.TextObjects
	Media              *media.Media
	Window             *app.Window
}

type Cards []*Card

func (cards Cards) Len() int {
	return len(cards)
}

func (cards Cards) Less(i, k int) bool {
	return cards[i].Item.PublishedParsed.After(*cards[k].Item.PublishedParsed)
}

func (cards Cards) Swap(i, k int) {
	cards[i], cards[k] = cards[k], cards[i]
}

func (card *Card) saveImage() func(image.Image) {
	return func(img image.Image) {
		card.ItemImage = img
		for _, vi := range onScreenCards {
			if vi == card {
				card.Window.Invalidate()
				break
			}
		}
	}
}

func (card *Card) OpenArticle() {
	if card == nil {
		return
	}
	if card.Article == nil {
		article, err := NewArticle(card.Item.Link, CLI.HTTPSettings.Client())
		if err != nil {
			log.Println("ERROR: scraping link:", err)
			return
		}
		card.Article = article
		card.ArticleRichContent, err = ParseArticleContent(card.Article)
		if err != nil {
			log.Println("ERROR: parsing article content:", err)
			card.ArticleRichContent = richtext.TextObjects{
				{
					Font:    fontCollection[0].Font,
					Color:   theme.Fg,
					Size:    unit.Dp(18),
					Content: card.Article.TextContent,
				},
			}
		}
	}
	if len(card.Article.TextContent) < len(card.Item.Description) {
		card.Article.TextContent = card.Item.Description
	}
	openedArticle.Article = card.Article
	openedArticle.Link = card.Item.Link
	openedArticle.RichContent = card.ArticleRichContent
	if openedArticle.Image != "" {
		imgDownloader.Download(
			openedArticle.Image,
			func(img image.Image) {
				openedArticle.topImage = img
				card.Window.Invalidate()
			},
		)
	}
	card.Window.Invalidate()
}

func (card *Card) GetMedia() (*media.Media, error) {
	if card == nil {
		return nil, nil
	}
	if card.Media == nil || len(card.Media.Links) == 0 {
		m, err := mediaExtractor.NewMedia(card.Item.Link)
		if err != nil {
			return nil, err
		}
		card.Media = m
	}
	return card.Media, nil
}

func (card *Card) RunMedia() {
	if card == nil {
		return
	}
	events.Emit(&events.RunMediaStart{Link: card.Item.Link})
	card.Window.Invalidate()
	go func() {
		defer func() {
			events.Emit(&events.RunMediaEnd{Link: card.Item.Link})
			card.Window.Invalidate()
		}()
		if _, err := card.GetMedia(); err != nil {
			log.Println("ERROR: extracting media link:", err)
			return
		}
		card.Media.Run()
	}()
}

func (card *Card) DownloadMedia() {
	if card == nil {
		return
	}
	go func() {
		log.Println("INFO: downloading media for:", card.Item.Link)
		if _, err := card.GetMedia(); err != nil {
			log.Println("ERROR: extracting media link:", err)
			return
		}
		if err := downloadLinks(card.Media.Links); err != nil {
			log.Println("ERROR: downloading media:", err)
			return
		}
	}()
}

func (card *Card) DownloadLink() {
	if card == nil {
		return
	}
	go func() {
		if err := downloadLinks([]string{card.Item.Link}); err != nil {
			log.Println("ERROR: downloading link:", err)
			return
		}
	}()
}

func (card *Card) DownloadImage() {
	if card == nil || card.Item == nil || card.Item.Image == nil {
		return
	}
	go func() {
		if err := downloadLinks([]string{card.Item.Image.URL}); err != nil {
			log.Println("ERROR: downloading image:", err)
			return
		}
	}()
}

func downloadLinks(links []string) error {
	//get download path
	downloadPath := CLI.DownloadPath
	if strings.Contains(downloadPath, "$HOME") {
		usr, err := user.Current()
		if err != nil {
			return err
		}
		downloadPath = strings.ReplaceAll(downloadPath, "$HOME", usr.HomeDir)
	}
	//create download path
	if _, err := os.Stat(downloadPath); os.IsNotExist(err) {
		if err := os.MkdirAll(downloadPath, 0x755); err != nil {
			return err
		}
	}
	for _, link := range links {
		//get response
		req, err := http.NewRequest("GET", link, nil)
		if err != nil {
			return err
		}
		client := CLI.HTTPSettings.Client()
		resp, err := client.Do(req)
		if err != nil {
			return err
		}
		defer resp.Body.Close()
		//get filename
		l, err := url.Parse(link)
		if err != nil {
			return err
		}
		filename := filepath.Base(l.Path)
		//if filename doesn't have an extension, guess by the content type
		if filepath.Ext(filename) == "" {
			contentType := resp.Header.Get("Content-Type")
			exts, err := mime.ExtensionsByType(contentType)
			if err != nil {
				return err
			}
			if len(exts) != 0 {
				filename += exts[0]
			}
		}
		//write data to file
		f, err := os.Create(filepath.Join(downloadPath, filename))
		if err != nil {
			return err
		}
		if _, err := io.Copy(f, resp.Body); err != nil {
			return err
		}
		if err := f.Close(); err != nil {
			return err
		}
		log.Printf("INFO: downloaded link %s to %s", link, filepath.Join(downloadPath, filename))
	}
	return nil
}

func (card *Card) OpenBrowser() {
	if card == nil {
		return
	}
	browser.OpenURL(card.Item.Link)
}

func (card *Card) Layout(gtx layout.Context, th *material.Theme) layout.Dimensions {
	// Process events that arrived between the last frame and this one.
	for _, ev := range gtx.Queue.Events(card.Item) {
		if x, ok := ev.(pointer.Event); ok {
			switch x.Type {
			case pointer.Press:
				go func() {
					switch {
					case x.Modifiers.Contain(key.ModCtrl):
						card.RunMedia()
						return
					case x.Modifiers.Contain(key.ModAlt):
						card.OpenBrowser()
						return
					}
					card.OpenArticle()
				}()
			}
		}
	}

	var flexChilds []layout.FlexChild

	imageHeight := int(float64(gtx.Constraints.Max.Y) * 0.8)
	titleHeight := gtx.Constraints.Max.Y - imageHeight

	//image
	if card.ItemImage != nil {
		flexChilds = append(flexChilds, layout.Rigid(
			func(gtx layout.Context) layout.Dimensions {
				gtx.Constraints.Min.Y = imageHeight
				gtx.Constraints.Max.Y = imageHeight
				return layout.Flex{Spacing: layout.SpaceAround}.Layout(
					gtx,
					layout.Rigid(card.ImageWidget()),
				)
			},
		))
	}

	//title label
	if card.Item.Image != nil && card.ItemImage == nil {
		flexChilds = append(
			flexChilds,
			layout.Rigid(
				func(gtx layout.Context) layout.Dimensions {
					return layout.Inset{
						Top: unit.Px(float32(imageHeight)),
					}.Layout(gtx, card.HeaderWidget(th))
				},
			),
		)
	} else {
		flexChilds = append(flexChilds, layout.Rigid(
			func(gtx layout.Context) layout.Dimensions {
				gtx.Constraints.Max.Y = titleHeight
				gtx.Constraints.Min.Y = titleHeight
				return card.HeaderWidget(th)(gtx)
			},
		))
	}

	//description
	if card.Item.Image == nil {
		flexChilds = append(flexChilds, layout.Rigid(
			func(gtx layout.Context) layout.Dimensions {
				gtx.Constraints.Min.Y = imageHeight
				gtx.Constraints.Max.Y = imageHeight
				return description(gtx, th, card.Item.Description)
			},
		))
	}

	if openedArticle.Article == nil {
		pointer.Rect(image.Rect(
			int(insetValue.V),
			int(insetValue.V),
			gtx.Constraints.Max.X,
			gtx.Constraints.Max.Y,
		)).Add(gtx.Ops)
		// Declare the tag.
		pointer.InputOp{
			Tag:   card.Item,
			Types: pointer.Move | pointer.Press,
		}.Add(gtx.Ops)
	}

	//surface and inset
	return layout.UniformInset(insetValue).Layout(
		gtx,
		func(gtx layout.Context) layout.Dimensions {
			elevation := unit.Dp(2)
			if card == selectedCard.Card {
				elevation = unit.Dp(6)
			}
			return Surface{Elevation: elevation}.Layout(
				gtx, th,
				func(gtx layout.Context) layout.Dimensions {
					return layout.Flex{Axis: layout.Vertical}.Layout(gtx, flexChilds...)
				},
			)
		},
	)
}
func (card *Card) HeaderWidget(th *material.Theme) layout.Widget {
	return func(gtx layout.Context) layout.Dimensions {
		return layout.Inset{Left: cardPadding, Right: cardPadding}.Layout(gtx,
			func(gtx layout.Context) layout.Dimensions {
				return layout.Flex{Axis: layout.Vertical}.Layout(gtx,
					layout.Rigid(card.HeaderInfo(th)),
					//item title
					layout.Flexed(1,
						func(gtx layout.Context) layout.Dimensions {
							label := material.Label(th, unit.Px(float32(gtx.Constraints.Max.Y)/2.5), card.Item.Title)
							label.Font.Weight = text.Bold
							return label.Layout(gtx)
						},
					),
				)
			},
		)
	}
}

func (card *Card) HeaderInfo(th *material.Theme) layout.Widget {
	return func(gtx layout.Context) layout.Dimensions {
		var flexChilds []layout.FlexChild
		//feed image
		if card.Feed.Image != nil {
			if card.FeedImage == nil {
				if img, ok := imgDownloader.GetImg(card.Feed.Image.URL); ok {
					card.FeedImage = img
				}
			}
			if card.FeedImage != nil {
				flexChilds = append(flexChilds, layout.Rigid(card.FeedImageWidget()))
			}
		}
		//feed title and autor name
		flexChilds = append(
			flexChilds,
			layout.Rigid(
				func(gtx layout.Context) layout.Dimensions {
					labels := []layout.FlexChild{
						layout.Rigid(
							func(gtx layout.Context) layout.Dimensions {
								label := material.Label(th, unit.Dp(12), card.Feed.Title)
								label.Color = shadeColor(label.Color, 80)
								return label.Layout(gtx)
							},
						),
					}
					if card.Item.Author != nil && card.Item.Author.Name != card.Feed.Title {
						labels = append(labels, layout.Rigid(
							func(gtx layout.Context) layout.Dimensions {
								label := material.Label(th, unit.Dp(12), card.Item.Author.Name)
								label.Color = shadeColor(label.Color, 80)
								return label.Layout(gtx)
							},
						))
					}
					if card.Feed.Image != nil {
						return layout.Inset{Left: cardPadding}.Layout(gtx,
							func(gtx layout.Context) layout.Dimensions {
								return layout.Flex{Axis: layout.Vertical}.Layout(gtx, labels...)
							},
						)
					} else {
						return layout.Flex{Axis: layout.Vertical}.Layout(gtx, labels...)
					}
				},
			),
		)
		//published time
		flexChilds = append(
			flexChilds,
			layout.Flexed(1,
				func(gtx layout.Context) layout.Dimensions {
					return layout.Flex{Spacing: layout.SpaceStart}.Layout(gtx,
						layout.Rigid(
							func(gtx layout.Context) layout.Dimensions {
								timeLabel := material.Label(
									th,
									unit.Dp(12),
									htime.Difference(time.Now(), *card.Item.PublishedParsed),
								)
								timeLabel.Color = shadeColor(timeLabel.Color, 80)
								return timeLabel.Layout(gtx)
							},
						),
					)
				},
			),
		)
		return layout.Inset{Top: cardPadding}.Layout(gtx,
			func(gtx layout.Context) layout.Dimensions {
				return layout.Flex{}.Layout(gtx, flexChilds...)
			},
		)
	}
}

func (card *Card) ImageWidget() layout.Widget {
	return func(gtx layout.Context) layout.Dimensions {
		if card.ItemImageOp.Size().X != gtx.Constraints.Max.X && card.ItemImageOp.Size().Y != gtx.Constraints.Max.Y {
			imgSize := card.ItemImage.Bounds().Size()
			var imgScale image.Point
			if imgSize.X > imgSize.Y {
				imgScale.X = gtx.Constraints.Max.X
				imgScale.Y = int(float32(imgSize.Y) * (float32(gtx.Constraints.Max.X) / float32(imgSize.X)))
			} else {
				imgScale.Y = gtx.Constraints.Max.Y
				imgScale.X = int(float32(imgSize.X) * (float32(gtx.Constraints.Max.Y) / float32(imgSize.Y)))
			}
			img := image.NewRGBA(image.Rectangle{Max: imgScale})
			draw.ApproxBiLinear.Scale(img, img.Bounds(), card.ItemImage, card.ItemImage.Bounds(), draw.Src, nil)
			card.ItemImageOp = paint.NewImageOp(img)
		}
		return widget.Image{
			Src:   card.ItemImageOp,
			Scale: float32(gtx.Constraints.Max.X) / float32(gtx.Px(unit.Dp(float32(gtx.Constraints.Max.X)))),
		}.Layout(gtx)
	}
}

func (card *Card) FeedImageWidget() layout.Widget {
	return func(gtx layout.Context) layout.Dimensions {
		feedImageSize := float32(gtx.Constraints.Max.Y) / 2
		gtx.Constraints.Max = image.Point{X: int(feedImageSize), Y: int(feedImageSize)}
		gtx.Constraints.Min = image.Point{X: int(feedImageSize), Y: int(feedImageSize)}
		if card.FeedImageOp.Size().X != gtx.Constraints.Max.X && card.FeedImageOp.Size().Y != gtx.Constraints.Max.Y {
			img := image.NewRGBA(image.Rectangle{Max: gtx.Constraints.Max})
			draw.ApproxBiLinear.Scale(img, img.Bounds(), card.FeedImage, card.FeedImage.Bounds(), draw.Src, nil)
			card.FeedImageOp = paint.NewImageOp(img)
		}
		clip.Circle{
			Center: f32.Point{X: feedImageSize / 2, Y: feedImageSize / 2},
			Radius: feedImageSize / 2,
		}.Add(gtx.Ops)
		return widget.Image{
			Src:   card.FeedImageOp,
			Scale: float32(gtx.Constraints.Max.X) / float32(gtx.Px(unit.Dp(float32(gtx.Constraints.Max.X)))),
		}.Layout(gtx)
	}
}

func description(gtx layout.Context, th *material.Theme, description string) layout.Dimensions {
	return layout.Inset{Left: cardPadding, Right: cardPadding}.Layout(gtx,
		material.Label(
			th,
			unit.Dp(14),
			description,
		).Layout,
	)
}
